Create pem-key pair and put them into `app/config/jwt`


Create `app/.env.local` file where you can overwrite any variable


For local development rename `docker-compose.local-overwrite.yml` to `docker-compose.overwrite.yml`

Start project with command
`docker-compose up -d --build`

If you want to load mock data for mannual testing, you can do it with command
`docker-compose exec php-fpm php bin/console doctrine:fixtures:load`
Be careful - this command clear DB

Swagger sandbox:
`/api/docs`
