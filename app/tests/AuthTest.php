<?php

namespace App\Tests;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\DataFixtures\TaskFixtures;
use App\DataFixtures\TaskListFixtures;
use App\DataFixtures\AUserFixtures;
use App\Entity\User;

class AuthTest extends BaseApiTestCase
{

    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $encoder = $kernel->getContainer()->get('security.password_encoder');
        $userFixtures = new AUserFixtures($encoder);
        $this->addFixture($userFixtures);

        $taskListsFixtures = new TaskListFixtures();
        $this->addFixture($taskListsFixtures);

        $taskFixtures = $kernel->getContainer()->get(TaskFixtures::class);
        $this->addFixture($taskFixtures);

        $this->executeFixtures();
    }

    public function testAuthentication(): void
    {
        $client = static::createClient();

        $manager = self::$container->get('doctrine')->getManager();
        $userRepo = $manager->getRepository(User::class);
        $user = $userRepo->findOneBy(['email' => 'test1@test.com']);

        // retrieve a token
        $response = $client->request('POST', '/authentication_token', [
            'headers' => ['Content-Type' => 'application/json'],
            'json' => [
                'email' => 'test1@test.com',
                'password' => 'test1',
            ],
        ]);

        $json = $response->toArray();
        self::assertResponseIsSuccessful();
        self::assertArrayHasKey('token', $json);

        // test not authorized
        $client->request('GET', "/api/v1/users/{$user->getId()}/task_lists");
        self::assertResponseStatusCodeSame(401);

        // test authorized
        $client->request('GET', "/api/v1/users/{$user->getId()}/task_lists", ['auth_bearer' => $json['token']]);
        self::assertResponseIsSuccessful();
    }

    public function testUsersCollection(): void
    {
        $userToken = $this->getUser1Token();
        $adminToken = $this->getAdminToken();

        $client = static::createClient();

        // test not authorized
        $client->request('GET', "/api/v1/users");
        self::assertResponseStatusCodeSame(401);

        // test forbidden
        $client->request('GET', "/api/v1/users", ['auth_bearer' => $userToken]);
        self::assertResponseStatusCodeSame(403);

        // test authorized
        $client->request('GET', "/api/v1/users", ['auth_bearer' => $adminToken]);
        self::assertResponseIsSuccessful();
    }

    public function testTaskListCollection(): void
    {
        $client = static::createClient();

        $userToken = $this->getUser1Token();
        $adminToken = $this->getAdminToken();

        $manager = self::$container->get('doctrine')->getManager();
        $userRepo = $manager->getRepository(User::class);
        $user1 = $userRepo->findOneBy(['email' => 'test1@test.com']);
        $user2 = $userRepo->findOneBy(['email' => 'test2@test.com']);

        $client = static::createClient();

        // test not authorized
        $client->request('GET', "/api/v1/users/{$user1->getId()}/task_lists");
        self::assertResponseStatusCodeSame(401);

        // test forbidden
        $client->request('GET', "/api/v1/users/{$user2->getId()}/task_lists", ['auth_bearer' => $userToken]);
        self::assertResponseStatusCodeSame(403);
    }

    private function getUserToken(string $email, string $password): string
    {
        $client = static::createClient();

        // retrieve a token
        $response = $client->request('POST', '/authentication_token', [
            'headers' => ['Content-Type' => 'application/json'],
            'json' => [
                'email' => $email,
                'password' => $password,
            ],
        ]);

        $json = $response->toArray();

        return $json['token'];
    }

    private function getUser1Token(): string
    {
        return $this->getUserToken('test1@test.com', 'test1');
    }

    private function getAdminToken(): string
    {
        return $this->getUserToken('admin@test.com', 'admin');
    }
}
