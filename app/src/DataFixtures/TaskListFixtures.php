<?php

namespace App\DataFixtures;

use App\Entity\TaskList;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class TaskListFixtures extends Fixture
{
    const TASK_LIST_1 = 'tl1';
    const TASK_LIST_2 = 'tl2';
    const TASK_LIST_3 = 'tl3';

    public function load(ObjectManager $manager): void {
        $user1 = $this->getReference(AUserFixtures::USER1);
        $user2 = $this->getReference(AUserFixtures::USER2);

        $tl1 = new TaskList();
        $tl1->setUser($user1);
        $tl1->setTitle('user1 task list 1');
        $manager->persist($tl1);

        $tl2 = new TaskList();
        $tl2->setUser($user1);
        $tl2->setTitle('user1 task list 2');
        $manager->persist($tl2);

        $tl3 = new TaskList();
        $tl3->setUser($user2);
        $tl3->setTitle('user2 task list 2');
        $manager->persist($tl3);

        $manager->flush();

        $this->addReference(self::TASK_LIST_1, $tl1);
        $this->addReference(self::TASK_LIST_2, $tl2);
        $this->addReference(self::TASK_LIST_3, $tl3);
    }

}
