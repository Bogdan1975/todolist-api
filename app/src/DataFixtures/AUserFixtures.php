<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AUserFixtures extends Fixture
{

    const USER1 = 'user1';
    const USER2 = 'user2';
    const ADMIN = 'admin';

    private UserPasswordEncoderInterface $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
//        $this->encoder = $encoder;
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager): void {
        $user1 = new User();
        $user1->setEmail('test1@test.com');
        $password = $this->encoder->encodePassword($user1, 'test1');
        $user1->setPassword($password);
        $user1->setRoles(['ROLE_USER']);
        $manager->persist($user1);

        $user2 = new User();
        $user2->setEmail('test2@test.com');
        $password = $this->encoder->encodePassword($user1, 'test2');
        $user2->setPassword($password);
        $user2->setRoles(['ROLE_USER']);
        $manager->persist($user2);

        $admin = new User();
        $admin->setEmail('admin@test.com');
        $password = $this->encoder->encodePassword($user1, 'admin');
        $admin->setPassword($password);
        $admin->setRoles(['ROLE_USER', 'ROLE_ADMIN']);
        $manager->persist($admin);

        $manager->flush();

        $this->addReference(self::USER1, $user1);
        $this->addReference(self::USER2, $user2);
        $this->addReference(self::ADMIN, $admin);
    }
}
