<?php

namespace App\DataFixtures;

use App\Entity\Task;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class TaskFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void {
        $tl1 = $this->getReference(TaskListFixtures::TASK_LIST_1);
        $tl2 = $this->getReference(TaskListFixtures::TASK_LIST_2);
        $tl3 = $this->getReference(TaskListFixtures::TASK_LIST_3);

        $faker = Factory::create();

        for ($j = 0; $j < 3; $j++) {
            for ($i = 0; $i < 10; $i++) {
                $task = new Task();
                $task->setTitle($faker->realText($faker->numberBetween(10, 30)));
                $task->setDescription($faker->realText($faker->numberBetween(30, 100)));
                $task->setList([$tl1, $tl2, $tl3][$j]);
                $task->setPriority($i + 1);
                $task->setFinished($faker->numberBetween(0, 3) === 0);
                $manager->persist($task);
            }
        }

        $manager->flush();
    }

    public function getDependencies(): array {
        return [
            TaskListFixtures::class,
        ];
    }

}
