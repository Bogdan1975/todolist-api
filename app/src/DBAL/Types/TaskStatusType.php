<?php

namespace App\DBAL\Types;

class TaskStatusType extends EnumType
{
    public const ENUM_TASK_STATUS = 'enumtaskstatus';
    public const STATUS_NEW = 'new';
    public const STATUS_IN_PROGRESS = 'in_progress';
    public const STATUS_FINISHED = 'finished';

    protected string $name = self::ENUM_TASK_STATUS;
    protected array $values = [self::STATUS_NEW, self::STATUS_IN_PROGRESS, self::STATUS_FINISHED];
}
